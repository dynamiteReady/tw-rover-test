Known issues:

- No tabs allowed in the input.
- No spaces or newlines at the beginning of input.
- Wasn't sure if the rovers could go out of bounds.
- Case sensitive.
- Everything in one module.
- Only tests exported functions (recent Elixir habit).

How to use:

Actually... There's no real API or UI.
It basically plugs straight into the tests...
The best you'll get is...

`npm install`
`npm test`
