"use strict";

const assert = require("assert");
const roverPositionProcessor = require("../rover-position-processor.js");
const testData = `5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM`;

let parsedTestData = {};

describe("Rover position processor", () => {
  describe("deserializeData: ", () => {
    beforeEach(() => {
      parsedTestData = roverPositionProcessor.deserializeData(testData);
    });

    it("should reject empty input", () => {
      assert(
        roverPositionProcessor.deserializeData() ===
          "Input should not be empty or start with a newline!"
      );
    });

    it("should reject files that start with newlines", () => {
      assert(
        roverPositionProcessor.deserializeData("\n") ===
          "Input should not be empty or start with a newline!"
      );
    });

    it("should reject files with invalid gridsize data", () => {
      assert(
        roverPositionProcessor.deserializeData("#") ===
          "Invalid grid size data!"
      );
    });

    it("should reject files that contain no rover position data", () => {
      assert(
        roverPositionProcessor.deserializeData("5 5") ===
          "Should contain rover position data!"
      );
    });

    it("should reject files that contain incomplete initial position tuples", () => {
      assert(
        roverPositionProcessor.deserializeData("3 3\n0 W\nL") ===
          "Position vector tuples should contain 3 elements!"
      );
    });

    it("should reject files which contain negative rover position values", () => {
      assert(
        roverPositionProcessor.deserializeData("`3 3\n0 -1 N\nL`") ===
          "No negative numbers allowed in initial rover position!"
      );
      assert(
        roverPositionProcessor.deserializeData("`3 3\n-1 0 N\nL`") ===
          "No negative numbers allowed in initial rover position!"
      );
    });

    it("should reject files with corrupt compass directions", () => {
      assert(
        roverPositionProcessor.deserializeData("3 3\n0 0 X\nL") ===
          "The rover heading value should be 'N', 'W', 'E' or 'S'!"
      );
    });

    it("should reject files with corrupt instuction sets", () => {
      assert(
        roverPositionProcessor.deserializeData("3 3\n0 0 N\nJJL") ===
          "The intructions can only contain the letters 'L', 'R' and 'M'!"
      );
    });

    it("should set the size of the grid", () => {
      assert(
        JSON.stringify(parsedTestData.gridSize) === JSON.stringify([5, 5])
      );
    });

    it("should add the correct number of rovers", () => {
      assert(parsedTestData.rovers.length === 2);
    });

    it("should set the initial position and heading of each rover", done => {
      parsedTestData.rovers.forEach(item => {
        assert(typeof item["initialPosition"][0] === "number");
        assert(typeof item["initialPosition"][1] === "number");
        assert(typeof item["initialPosition"][2] === "string");
      });
      done();
    });

    it("should create an instruction array for each rover", done => {
      parsedTestData.rovers.forEach(item => {
        assert(item["instructions"].length > 0);
      });
      done();
    });
  });

  describe("processRoverPositions: ", () => {
    it("should not move a rover out of the grid boundary", () => {
      const response = roverPositionProcessor.processRoverPositions(
        "3 3\n1 2 N\nMMMMMMMM"
      );
      assert(
        response ===
          "The rover went out of bounds! Check those instructions again!"
      );
    });

    it("should determine new heading for a given rover when 'L' is specified", () => {
      const response = roverPositionProcessor.processRoverPositions(
        "3 3\n0 0 N\nL"
      );
      assert(response === "0 0 W");
    });

    it("should determine new heading for a given rover when 'R' is specified", () => {
      const response = roverPositionProcessor.processRoverPositions(
        "3 3\n0 0 N\nR"
      );
      assert(response === "0 0 E");
    });

    it("should cycle through all compass points, when applying 'L'", () => {
      const response = roverPositionProcessor.processRoverPositions(
        "3 3\n0 0 N\nLLLLL"
      );
      assert(response === "0 0 W");
    });

    it("should cycle through all compass points, when applying 'R'", () => {
      const response = roverPositionProcessor.processRoverPositions(
        "3 3\n0 0 N\nRRRRR"
      );
      assert(response === "0 0 E");
    });

    it("should move a rover forward in the given direction when 'M' is specified", () => {
      const response = roverPositionProcessor.processRoverPositions(
        "3 3\n2 2 N\nM"
      );
      assert(response === "2 3 N");
    });

    it("should output a new line for each rover entity", () => {
      const response = roverPositionProcessor.processRoverPositions(
        "3 3\n0 0 N\nM\n0 0 N\nM\n0 0 N\nM"
      );
      assert(response === "0 1 N\n0 1 N\n0 1 N");
    });

    it("should output the correct final position for each rover", () => {
      const response = roverPositionProcessor.processRoverPositions(testData);
      assert(response === "1 3 N\n5 1 E");
    });
  });
});
