const vectorMap = ["N", "E", "S", "W"];

const setGridSize = data => {
  // Basic split on space, and cast to integers...
  return data.split(" ").map(item => parseInt(item));
};

const isOutOfBounds = (currentVector, grid) => {
  if (
    currentVector[0] < 0 ||
    currentVector[0] > grid[0] ||
    (currentVector[1] < 0 || currentVector[1] > grid[1])
  ) {
    return true;
  } else return false;
};

const formatOutput = finalResult => {
  return finalResult
    .map(item => {
      return item.toString().replace(/,/g, " ");
    })
    .toString()
    .replace(/,/g, "\n");
};

const calculateNewVector = (currentVector, instruction) => {
  let newVector = currentVector;
  let currentHeading = vectorMap.indexOf(currentVector[2]);

  // If a dumb character is fed in, stop...
  if (currentHeading < 0) return false;

  switch (instruction) {
    case "L":
      currentHeading--;
      if (currentHeading < 0) currentHeading = vectorMap.length - 1;
      break;

    case "R":
      currentHeading++;
      if (currentHeading > vectorMap.length - 1) currentHeading = 0;
      break;

    case "M":
      newVector = setPosition(currentVector);
      break;

    default:
      break;
  }

  newVector[2] = vectorMap[currentHeading];

  return newVector;
};

const setPosition = currentVector => {
  let newVector = currentVector;
  // N = y++ = 0
  // E = x++ = 1
  // S = y-- = 2
  // W = x-- = 3
  switch (currentVector[2]) {
    case "N":
      newVector[1] = newVector[1] + 1;
      break;

    case "E":
      newVector[0] = newVector[0] + 1;
      break;

    case "S":
      newVector[1] = newVector[1] - 1;
      break;

    case "W":
      newVector[0] = newVector[0] - 1;
      break;

    default:
      break;
  }

  return newVector;
};

const parseRoverData = data => {
  let isInvalidCompassPoint = false;
  let isNegativeNumber = false;
  let containsInvalidInstructions = false;
  let containsIncompleteVector = false;

  const parsedRover = data.reduce((accum, item, index) => {
    if (index == 0 || index % 2 == 0) {
      // For the first item, and every successive even index value,
      // add a new rover object, then process the `initialPosition` value.
      accum.push({});
      accum[accum.length - 1]["initialPosition"] = item
        .split(" ")
        .map((item, index, array) => {
          if (index < array.length - 1) {
            const parsedValue = parseInt(item);
            if (!isNegativeNumber && parsedValue < 0) isNegativeNumber = true;
            return parsedValue;
          } else {
            if (!isInvalidCompassPoint && !vectorMap.includes(item))
              isInvalidCompassPoint = true;
            return item;
          }
        });

      if (
        !containsIncompleteVector &&
        accum[accum.length - 1]["initialPosition"].length < 3
      ) {
        containsIncompleteVector = true;
      }
    } else {
      // For odd index values, set the rover's instructions.
      accum[accum.length - 1]["instructions"] = item.split("");
      if (!containsInvalidInstructions) {
        containsInvalidInstructions =
          item.split("").length !==
          accum[accum.length - 1]["instructions"].filter(
            item => item == "L" || item == "R" || item == "M"
          ).length;
      }
    }

    return accum;
  }, []);

  if (isNegativeNumber) {
    return "No negative numbers allowed in initial rover position!";
  } else if (containsInvalidInstructions) {
    return "The intructions can only contain the letters 'L', 'R' and 'M'!";
  } else if (isInvalidCompassPoint) {
    return "The rover heading value should be 'N', 'W', 'E' or 'S'!";
  } else if (containsIncompleteVector) {
    return "Position vector tuples should contain 3 elements!";
  }

  return parsedRover;
};

const roverPositionProcessor = {
  deserializeData(raw = "") {
    const data = raw.split("\n");
    let parsedData = {};

    // If the input is empty
    if (!data.length || !data[0].length)
      return "Input should not be empty or start with a newline!";

    // Set gridsize
    parsedData.gridSize = setGridSize(data.shift());

    // In case there's no match pos/instruction pairing
    if (data.length % 2 > 0)
      return "Each rover position tuple should have an accompanying instruction set!";

    // Set rover data
    parsedData.rovers = parseRoverData(data);

    // Ghetto error boundary...
    if (typeof parsedData.rovers == "string") {
      return parsedData.rovers;
    } else if (
      !parsedData.gridSize[0] ||
      parsedData.gridSize[0] < 0 ||
      !parsedData.gridSize[1] ||
      parsedData.gridSize[1] < 0
    ) {
      return "Invalid grid size data!";
    } else if (!parsedData.rovers.length) {
      return "Should contain rover position data!";
    }

    return parsedData;
  },

  processRoverPositions(data) {
    const roverData = this.deserializeData(data);

    if (typeof roverData === "string") return roverData;

    let outOfBounds = false;
    const finalRoverPositions = roverData.rovers.map(item => {
      let roverPosition = item.initialPosition;
      outOfBounds = isOutOfBounds(roverPosition, roverData.gridSize);

      item["instructions"].forEach(instruction => {
        roverPosition = calculateNewVector(roverPosition, instruction);
        outOfBounds = isOutOfBounds(roverPosition, roverData.gridSize);
      });

      return roverPosition;
    });

    if (outOfBounds)
      return "The rover went out of bounds! Check those instructions again!";

    return formatOutput(finalRoverPositions);
  }
};

module.exports = roverPositionProcessor;
